pragma solidity ^0.4.19;

import './zeppelin-solidity/contracts/token/PausableToken.sol';

contract CrypStock is PausableToken {
	string  public  name       = "CrypStock";
	string  public  symbol     = "CPSK";
	uint    public  decimals   = 18;

	function CrypStock(uint256 initBalance) {
		balances[msg.sender] = totalSupply = initBalance;
	}
}
