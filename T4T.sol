pragma solidity ^0.4.18;

import './zeppelin-solidity/contracts/token/PausableToken.sol';
import './zeppelin-solidity/contracts/token/MintableToken.sol';

contract T4T is PausableToken, MintableToken {
	string  public  name       = "Token4Trade";
	string  public  symbol     = "T4T";
	uint    public  decimals   = 18;

	function T4T() {
		totalSupply = 0;
	}
}
